#!/usr/local/bin/python3

import re
import codecs
from datetime import datetime
import sys
import os


def contentsOfFile(path):
	with codecs.open(path, 'r', 'utf-8') as file:
		data = file.read()
	return data

def write(path, data):
	with codecs.open(path, 'w', 'utf-8') as file:
		file.write(data)

def capFirstChar(s):
	return s[0].upper() + s[1:]

from json import JSONEncoder
class MyEncoder(JSONEncoder):
	def default(self, o):
		return o.__dict__


#count = len(re.findall(old, data, flags=re.MULTILINE))
#data = re.sub(old, new, data, flags=re.MULTILINE)


class Class(object):
	"""docstring for Class"""

	name = None

	def __init__(self, filePath):
		super(Class, self).__init__()

		self.header = filePath
		self.implementation = filePath[:-2] + '.m'
		self.name = self.header.split('/')[-1:][0][:-2]
		self.dir = os.path.dirname(self.header)

		dataH = contentsOfFile(self.header)
		self.creator = getCreator(dataH)
		self.properties = Property.parse(dataH)

		self.protocols = self.parseProtocolsFromHeader(dataH)
		for protocol in self.protocols:
			pfile = os.path.join(self.dir, protocol+'.h')
			pdata = contentsOfFile(pfile)
			print('\n\nParsing protocol '+pfile+'\n\n'+pdata+'\n\n')
			self.properties += Property.parse(pdata)

		if not os.path.isfile(self.implementation):
			self.methods = Method.parse(dataH)
			return

		dataM = contentsOfFile(self.implementation)

		self.imports = []
		#self.methods = Method.parse(dataM, Method.parse(dataH))
		self.methods = Method.parse(dataM, 1)
		self.superClass = []

		methods = []
		for m in self.methods:
			if m.signature not in [x.name for x in self.properties]:
				methods.append(m)
		self.methods = methods

		self.load()

	def parseProtocolsFromHeader(self, dataH):
		r = r'@interface.*<([^>]+)>'
		m = re.search(r, dataH, flags=re.MULTILINE)
		if m is not None:
			return m.group(1).split(', ')
		return []


	def load(self):
		print('load ' + self.name)

	def methodForCall(self, call):
		print("\n\nSearching method for call " + call)
		for m in self.methods:
			print('??? ',Method.cleanSignature(call),' == ',Method.cleanSignature(m.signature))
			if Method.cleanSignature(m.signature) == Method.cleanSignature(call):
				print("Found: " + m.signature)
				return m
				print("\n\n")
		print("<<<<<<< NOT FOUND")


class Import(object):
	def __init__(self, isOld, className, lib = None):
		super(Import, self).__init__()

		self.className = className
		self.lib = lib
		self.isLocal = lib is None
		self.isOld = isOld

	def ImportsFromFile(path):
		return []


class Var(object):
	"""docstring for Var"""
	def __init__(self, _type, name):
		super(Var, self).__init__()
		self.type = _type
		self.typeForVariable = self.type + ('' if self.type[-1] == '*' else ' ')
		self.name = name
		self.capName = capFirstChar(name)
		self.className = self.type.replace(' *','')

		protocolMatch = re.match(r'id<(.*)>', self.type)
		self.protocol = None if not protocolMatch else protocolMatch[1]

		self.fileName = self.protocol if self.protocol else self.className

	def parseParamsFromMethodSignature(signature):
		argPattern = r':\((.*?)\)(\S*)'

		matches = re.findall(argPattern, signature, flags=re.MULTILINE)
		params = []
		for m in matches:
			param = Var(m[0],m[1])
			params.append(param)

		return params

	def mockCreation(self):
		if self.protocol is not None:
			return f'OCMStrictProtocolMock(@protocol({self.protocol}))'
		else:
			return f'OCMStrictClassMock([{self.className} class])'

	def mockCreationLine(self):
		return f'\t{self.typeForVariable}{self.name} = {self.mockCreation()};'


class Property(Var):
	"""docstring for Var"""
	def __init__(self, _type, name, modifiers):
		super(Property, self).__init__(_type, name)
		self.modifiers = modifiers

	def parse(data):
		matches = re.findall(r'@property \((.*)\) (.* \*?)(.*);', data, flags=re.MULTILINE)
		properties = []
		for m in matches:
			modifiers = m[0].split(', ')
			prop = Property(m[1],m[2],modifiers)
			properties.append(prop)

		return properties


class Method(object):
	def __init__(self, returnType, signature, body):
		super(Method, self).__init__()
		self.returnType = returnType
		self.signature = signature
		self.params = Var.parseParamsFromMethodSignature(self.signature)
		self.body = body

		self.capName = capFirstChar(signature.split(':')[0])

	def call(self):
		signatureWithoutParamsTypes = re.sub(r'\(.*?\)', '', self.signature)

		paramsmocks = [
			(f'\t{p.typeForVariable}{p.name} = {p.mockCreation()};') 
				for p in self.params
		];
		paramsmocks = '\n'.join(paramsmocks)

		importsFiles = [p.fileName for p in self.params]

		return signatureWithoutParamsTypes, paramsmocks, importsFiles


	def parse(data, headerDefined = None):
		'''
		headerDefined == None: 			for parsing .h file for methods
		headerDefined == 1: 			for parsing .m file for methods
		headerDefined == [Method]: 		for parsing .m file for methods only if they are defined in [Method]
											actually [Methods] is list of methods parsed from .h file
		'''
		pattern = r'- \((.*?)\)([^;]+)'
		if not headerDefined: # searching in header
			pattern = pattern + r';$'
		else:
			pattern = pattern + r'$\s{\s([\s\S]*?^)}'

		matches = re.findall(pattern, data, flags=re.MULTILINE)
		methods = []
		for m in matches:
			returnType = m[0]
			signature = m[1]
			body = m[2] if len(m)==3 else None
			meth = Method(returnType, signature, body)

			if (not headerDefined or headerDefined == 1) or (signature in [x.signature for x in headerDefined]):
				methods.append(meth)

		return methods

	def cleanSignature(callOrSignature):
		r = r'\(.*?\)'
		noTypes = re.sub(r, '', callOrSignature)
		r = r':\S+'
		noArgs = re.sub(r, '', noTypes)
		r = r'\s{2,}'
		noNewline = re.sub(r, ' ', noArgs)
		return noNewline


def testForLazyProperty(p, containerName, containerInstanceName):
	return f'''\
- (void)testThat{containerName}Creates{p.capName}Lazy
{{
	// arrange
	self.{containerInstanceName}.{p.name} = nil;
	
	// act
	{p.type}received = self.{containerInstanceName}.{p.name};
	
	// assert
	expect(received).to.beMemberOf([{p.type.split(' ')[0]} class]);
}}'''

def testCaseInterface(c):
	caseName = f'{c.name}Tests'
	properties = [f'@property (nonatomic, strong) {x.typeForVariable}{x.name}Mock;' for x in c.properties
		if not 'assign' in x.modifiers]

	properties = '\n'.join(properties)
	return f'''@interface {c.name}Tests : XCTestCase

@property (nonatomic, strong) {c.name} *testable;

{ properties }

@end
	'''

def setUpMethod(c, containerInstanceName):
	mocks = []
	nils = []

	for x in c.properties:
		if 'readonly' in x.modifiers or 'assign' in x.modifiers:
			continue

		mock = f'''	
	self.{x.name}Mock = {x.mockCreation()};
	self.{containerInstanceName}.{x.name} = self.{x.name}Mock;
	'''
		mocks.append(mock)
		nils.insert(0, f'''	self.{x.name}Mock = nil;''')

	mocks = ''.join(mocks)
	nils = '\n'.join(nils)

	return f'''- (void)setUp
{{
    [super setUp];

    self.{containerInstanceName} = [{c.name} new];
{mocks}
}}

- (void)tearDown
{{
{nils}

	self.{containerInstanceName} = nil;

    [super tearDown];
}}
'''


def parseCalls(body, cla):
	calls = []

	# replace "a.b = c;" with "[a setB:c];"
	r = r'(\S+)\.(\S*?) *= *(.*?);'
	rep = '[{} set{}:{}];'
	body = re.sub(r, lambda m: rep.format( m.group(1), capFirstChar(m.group(2)), m.group(3) ), body)

	for m in re.finditer(r'\[(\S+) (.*?(?:\[.*?\].*?)*)\]', body, flags = re.MULTILINE | re.DOTALL):
		callee = m[1]
		method = m[2]

		# вложенные вызовы внутри строки вызова
		calls = calls + parseCalls(method, cla)

		# ищем вызовы внутри методов, вызываемых в body
		if 'self' == callee:
			testablesMethod = [x for x in cla.methods if x.signature == method]
			if testablesMethod != []:
				insideCalls = parseCalls(testablesMethod[0].body, cla)
				calls += insideCalls
				print('\n for', method , ' ADDED INSIDE CALLS', insideCalls)

		calls.append([callee, method])
	return calls

parsedFiles = {}
def parseFile(dir, fileName):
	'''
	e.g. 'MapperProtocol'
	'''
	filePath = os.path.join(dir,fileName+'.h')

	if not os.path.isfile(filePath):
		return None

	if not filePath in parsedFiles:
		parsedFiles[filePath] = Class(filePath)

		print(os.path.split(filePath)[1])
		print(len(parsedFiles[filePath].methods))

	return parsedFiles[filePath]


def expectsForCalls(method, cla):
	expects = []
	mocksForReturns = []
	verifies = {}
	calls = parseCalls(method.body, cla)
	for call in calls:
		callee = call[0]
		method = call[1]
		if 'self.' in callee:
			callee = callee.replace('self.', '')

			calleeProperty = [x for x in cla.properties if x.name == callee]
			calleeProperty = calleeProperty[0] if len(calleeProperty) > 0 else None
			calleeType = calleeProperty.type if calleeProperty else None

			# если объект вызова является полем тестируемого класса
			if calleeType is not None:

				andReturn = ''

				# парсим файл, чтобы знать тип возвращаемого значения
				calleeClass = parseFile(cla.dir, calleeProperty.fileName)
				if calleeClass:
					calleeMethod = calleeClass.methodForCall(method)
					returnType = calleeMethod.returnType

					if returnType != 'void':
						mockName = calleeMethod.signature.split(':')[0]
						mockForReturnValue = Var(calleeMethod.type, mockName).mockCreationLine()
						mocksForReturns.append(mockForReturnValue)
						andReturn = f'.andReturn({mockName})'

				expect = f'	OCMExpect([self.{callee}Mock {method}]){andReturn};'
				expects.append(expect)
				verifies[f'self.{callee}Mock'] = True

	verifies = [f'	OCMVerifyAll((id){x});' for x in verifies]
	return '\n'.join(expects), '\n'.join(verifies)


def testsForMethods(c, containerInstanceName):
	tests = []
	imports = []
	for m in c.methods:
		expects, verifies = expectsForCalls(m, c)

		receivedVar = ''
		if m.returnType != 'void':
			t = m.returnType
			if t == 'instancetype':
				t = f'{c.name} *'
			if not ' ' in t:
				t = t + ' '
			receivedVar = f'{t}received = '

		m_call, paramsMockLocalVariables, importsFiles = m.call()

		imports += importsFiles

		testName = capFirstChar(m.signature.split(':')[0])

		methodTest = f'''
- (void)testThat{testName}
{{
	// arrange
{expects}

{paramsMockLocalVariables}

	// act
	{receivedVar}[self.{containerInstanceName} {m_call}];

	// assert
{verifies}	
}}
'''
		tests.append(methodTest)
	return tests, list(set(imports))


#####################################

def getCreator(data):
	r = r'Created by (.*) on'
	m = re.search(r, data)[1]
	if not m:
		return '<#CREATOR#>'
	return m

def fileHeader(file, creator):
	fileName = file.split('/')[-1]
	framework = file.split('/')[-2].replace('Framework', '')
	date = datetime.now().strftime("%d/%m/%Y")
	year = datetime.now().strftime("%Y")

	return f'''//  
//  {fileName}
//  {framework}
//
//  Created by {creator} on {date}.
//  Copyright © {year} SberTech. All rights reserved.
//'''

def importsForFiles(files):
	return '\n'.join(set([f'#import "{f}.h"' for f in files if not (f[:2]=='CG' or f[:2]=='NS')]))

def importsFilesForProperties(c):
	files = [p.fileName for p in c.properties]
	return files

def generateTestsForFile(file):
	if file.split('.')[-1] != 'h':
		print(f'Skipping not an .h file - {file}')
		return

	testsFilePath = file[:-2].replace('Framework', 'Tests') + 'Tests.m'
	if os.path.isfile(testsFilePath):
		print('Error: "' + testsFilePath + '"" file already exists!')
		#return

	c = Class(file)

	if c.name is None:
		print(f'Skipping (no class) file - {file}')
		return

	imports = '@import XCTest;\n@import OCMock;\n@import Expecta;'
	tests = []
	containerName = 'Testable'
	containerInstanceName = 'testable'
	### GENERATE LAZY CHECKS ###
	for prop in c.properties:
		if 'null_resettable' in  prop.modifiers:
			test = testForLazyProperty(prop, containerName, containerInstanceName)
			tests.append(test)
	tests = '\n\n'.join(tests) + '\n\n'

	setup = setUpMethod(c, containerInstanceName)

	interface = testCaseInterface(c)

	methodsTesting, importsFilesForTests = testsForMethods(c, containerInstanceName)
	tests += '\n\n'.join(methodsTesting)

	importsFiles = [c.name] + importsFilesForProperties(c) + importsFilesForTests
	imports += '\n\n'+importsForFiles(importsFiles)

	header = fileHeader(testsFilePath, c.creator)

	write(testsFilePath, '\n\n'.join([
		header,
		f'{imports}\n\n',
		interface, 
		f'@implementation {c.name}Tests\n',
		'#pragma mark - Setting up the test environment', 
		setup, 
		'#pragma mark - Tests', 
		tests,
		'@end'
		]))

files = sys.argv[1:]
#files = [ '../PTPODisclosableItem.h' ]
for file in files:
	generateTestsForFile(file)



